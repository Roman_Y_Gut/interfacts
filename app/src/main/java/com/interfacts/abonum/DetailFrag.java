package com.interfacts.abonum;

import static android.content.Context.MODE_PRIVATE;

import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


public class DetailFrag extends Fragment {

    private View viewDetail;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        viewDetail=inflater.inflate(R.layout.fragment_detail, container, false);
        setDetail();
        return viewDetail;
    }
    private void setDetail(){
        TextView detail_text = viewDetail.findViewById(R.id.detail_text);
        TextView detail_number = viewDetail.findViewById(R.id.detail_number);
        TextView detail_date = viewDetail.findViewById(R.id.detail_date);
        ImageView back_btn = viewDetail.findViewById(R.id.back_btn);
        if (requireActivity().getSharedPreferences("preferences",MODE_PRIVATE).getInt("itemId",0)!=0){
            int itemNumber = requireActivity().getSharedPreferences("preferences", MODE_PRIVATE).getInt("itemNumber", 0);
            String itemText = requireActivity().getSharedPreferences("preferences", MODE_PRIVATE).getString("itemText", "");
            String itemDate = requireActivity().getSharedPreferences("preferences", MODE_PRIVATE).getString("itemDate", "");
            detail_text.setText(itemText);
            detail_number.setText(String.valueOf(itemNumber));
            detail_date.setText(itemDate);
        }

        back_btn.setOnClickListener(view -> moveToMain());
        requireActivity().getOnBackPressedDispatcher().addCallback(new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                moveToMain();
            }
        });
    }
    private void moveToMain(){
        requireActivity().getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(android.R.animator.fade_in,android.R.animator.fade_out)
                .replace(R.id.frame_holder,new MainFrag()).commit();
    }
}