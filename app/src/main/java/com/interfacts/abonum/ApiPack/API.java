package com.interfacts.abonum.ApiPack;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface API {
    @GET("/random/math")
    Observable<ResponseBody> getRand();
    @GET("/{number}")
    Observable<ResponseBody> getNumber(@Path("number") int number);
}
