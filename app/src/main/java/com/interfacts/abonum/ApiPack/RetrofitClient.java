package com.interfacts.abonum.ApiPack;

import android.annotation.SuppressLint;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private static Retrofit retroInstance;
    @SuppressLint("SuspiciousIndentation")
    public static Retrofit getRetroInstance(){
        if (retroInstance==null)
            retroInstance=new Retrofit.Builder()
                    .baseUrl("http://numbersapi.com")
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
            return retroInstance;
    }
}
