package com.interfacts.abonum;

import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.interfacts.abonum.ApiPack.API;
import com.interfacts.abonum.ApiPack.RetrofitClient;
import com.interfacts.abonum.Database.Item;
import com.interfacts.abonum.Database.ItemAdapter;
import com.interfacts.abonum.Database.ItemViewModel;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;


public class MainFrag extends Fragment {

    private View viewMainFrag;
    private Button get_rand_btn,get_my_btn;
    private TextView rand_num_text;
    private EditText input_value;
    private ItemViewModel itemViewModel;
    private ItemAdapter itemAdapter;
    private API api;
    private final CompositeDisposable compositeDisposable=new CompositeDisposable();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        viewMainFrag=inflater.inflate(R.layout.fragment_main, container, false);
        Retrofit retrofit= RetrofitClient.getRetroInstance();
        api=retrofit.create(API.class);
        setMain();
        setMainListeners();
        return viewMainFrag;
    }
    private void setMainListeners(){
        get_rand_btn.setOnClickListener(view -> {
            setButtonsOff();
            fetchRand();
        });
        get_my_btn.setOnClickListener(view -> {
            if (!input_value.getText().toString().isEmpty()){
                setButtonsOff();
                fetchNumber();
            }
        });
        requireActivity().getOnBackPressedDispatcher().addCallback(new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                requireActivity().finish();
            }
        });
    }

    @Override
    public void onStop() {
        compositeDisposable.clear();
        super.onStop();
    }
    private void setMain(){
        input_value=viewMainFrag.findViewById(R.id.input_value);
        rand_num_text=viewMainFrag.findViewById(R.id.rand_num_text);
        get_rand_btn=viewMainFrag.findViewById(R.id.get_rand_btn);
        get_my_btn=viewMainFrag.findViewById(R.id.get_my_btn);
        RecyclerView main_recycler = viewMainFrag.findViewById(R.id.main_recycler);
        main_recycler.setHasFixedSize(true);
        main_recycler.setLayoutManager(new LinearLayoutManager(requireContext()));
        itemAdapter=new ItemAdapter(requireContext());
        itemViewModel= ViewModelProviders.of(requireActivity()).get(ItemViewModel.class);
        itemViewModel.getAllItems().observe(getViewLifecycleOwner(),
                items -> itemAdapter.setItems(items));
        main_recycler.setAdapter(itemAdapter);
    }
    private void fetchRand(){
        compositeDisposable.add(api.getRand()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::setAndSave));
    }
    private void fetchNumber(){
        compositeDisposable.add(api.getNumber(Integer.parseInt(input_value.getText().toString()))
                .subscribeOn(Schedulers.io())
                .subscribe(this::setAndSave));
    }
    private void setAndSave(ResponseBody resp) throws Exception{
        String responseStr =resp.string();
        requireActivity().runOnUiThread(() -> rand_num_text.setText(responseStr));
        Log.e("rgerg",responseStr);
        String []a1=responseStr.split(" ");
        setButtonsOn();
        itemViewModel.insertItem(new Item(Integer.parseInt(a1[0]),responseStr,getDate()));
    }
    private void setButtonsOff(){
        get_rand_btn.setClickable(false);
        get_my_btn.setClickable(false);
        get_my_btn.setAlpha(0.5f);
        get_rand_btn.setAlpha(0.5f);
    }
    private void setButtonsOn(){
        get_rand_btn.setClickable(true);
        get_my_btn.setClickable(true);
        get_my_btn.setAlpha(1.0f);
        get_rand_btn.setAlpha(1.0f);
    }
    private String getDate(){
        return new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
    }
}