package com.interfacts.abonum;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class MainHolder extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(android.R.anim.fade_in,android.R.anim.fade_out)
                .replace(R.id.frame_holder,new MainFrag()).commit();
    }
}