package com.interfacts.abonum.Database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface DAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertItem(Item numberItem);

    @Query("SELECT * FROM NumberItems")
    LiveData<List<Item>> getAllItems();
}
