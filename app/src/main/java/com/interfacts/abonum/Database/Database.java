package com.interfacts.abonum.Database;

import android.content.Context;

import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

@androidx.room.Database(entities = Item.class,version = 1,exportSchema = false)
public abstract class Database extends RoomDatabase {
    private static volatile Database database;
    public abstract DAO dao();
    public static Database getDatabase(final Context context){
        if (database==null){
            synchronized (Database.class){
                if (database==null){
                    database= Room.databaseBuilder(context.getApplicationContext(),Database.class,"items_database").build();
                }
            }
        }
        return database;
    }
}
