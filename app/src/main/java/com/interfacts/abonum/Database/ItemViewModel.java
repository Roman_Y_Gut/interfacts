package com.interfacts.abonum.Database;

import android.app.Application;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class ItemViewModel extends AndroidViewModel {

    private final DAO dao;
    private final LiveData<List<Item>> allItems;

    public ItemViewModel(@NonNull Application application) {
        super(application);
        Database database = Database.getDatabase(application);
        dao= database.dao();
        allItems=dao.getAllItems();
    }
    public void insertItem(Item itemInsert){
        new InsertItem(dao).execute(itemInsert);
    }
    public LiveData<List<Item>> getAllItems(){return allItems;}
    private static class InsertItem extends AsyncTask<Item,Void,Void>{
        DAO dao;
        InsertItem (DAO dao){
            this.dao=dao;
        }

        @Override
        protected Void doInBackground(Item... items) {
            dao.insertItem(items[0]);
            return null;
        }
    }
}
