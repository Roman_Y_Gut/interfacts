package com.interfacts.abonum.Database;

import static android.content.Context.MODE_PRIVATE;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.interfacts.abonum.DetailFrag;
import com.interfacts.abonum.R;

import java.util.List;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ItemHolder> {

    private final Context modelContext;
    private List<Item> itemsList;

    @NonNull
    @Override
    public ItemAdapter.ItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(modelContext).inflate(R.layout.item_lay,parent,false);
        return new ItemHolder(view);
    }
    public ItemAdapter(Context context){
        this.modelContext=context;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ItemAdapter.ItemHolder holder, int position) {
        final Item oneItem=itemsList.get(position);
        int itemId=oneItem.getId();
        int itemNumber=oneItem.getNumber();
        String itemText=oneItem.getText();
        String itemDate=oneItem.getDate();
        holder.itemView.setOnClickListener(view -> {
            AppCompatActivity goActivity=(AppCompatActivity) view.getContext();
            SharedPreferences preferences = goActivity.getSharedPreferences("preferences",MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putInt("itemId",itemId);
            editor.putInt("itemNumber",itemNumber);
            editor.putString("itemText",itemText);
            editor.putString("itemDate",itemDate);
            goActivity.getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(android.R.animator.fade_in,android.R.animator.fade_out)
                    .replace(R.id.frame_holder,new DetailFrag()).addToBackStack(null).commit();
            editor.apply();
        });
        holder.item_number.setText(String.valueOf(itemNumber));
        holder.item_text.setText(itemText);
        holder.item_date.setText(itemDate);

    }

    @Override
    public int getItemCount() {
        if (itemsList!=null){
            return itemsList.size();
        }else return 0;
    }
    @SuppressLint("NotifyDataSetChanged")
    public void setItems(List<Item> itemsList){
        this.itemsList=itemsList;
        notifyDataSetChanged();
    }

    public static class ItemHolder extends RecyclerView.ViewHolder {
        private final TextView item_number;
        private final TextView item_text;
        private final TextView item_date;
        public ItemHolder(@NonNull View itemView) {
            super(itemView);
            item_number=itemView.findViewById(R.id.item_number);
            item_text=itemView.findViewById(R.id.item_text);
            item_date=itemView.findViewById(R.id.item_date);
        }
    }
}
